# Desafío EterSoccer
 
Bienvenidos! En este desafío te retamos a crear tu propio equipo de fútbol usando programación y competir con otros participantes.
 
- [Introducción](#introducción)
   - [Requerimientos](#requerimientos)   
- [Instalación](#instalación)
- [Prueba un partido](#prueba-un-partido)
- [Construye tu equipo](#construye-tu-equipo)
- [Publish your team](#publish-your-team)
- [API](#api)
   - [TeamPlayer class](#teamplayer-class)
       - [Override methods](#override-methods)
       - [Public methods](#public-methods)
   - [Interfaces](#interfaces)
       - [Field Position](#fieldposition)
       - [Shoot Force](#shootforce)
   - [Examples](#examples)
 
## Introducción
Etermax presenta el Desafío EterSoccer, esta competencia tuvo origen como evento interno que se celebró dos veces en un año. El objetivo del evento es programar la inteligencia artificial de los jugadores, usando tácticas, jugadas, mentalidad de equipo o individual, etc.
La participación fue en grupos de a 2 a 4 personas y el ganador fue definido por eliminación directa, todo el evento fue transmitido en el auditorio de la oficina de Buenos Aires, generando una emocionante convocatoria.
Ahora queremos extender el proyecto hacia afuera como actividad didáctica ya que descubrimos que es una forma muy divertida de dar los primeros pasos en la programación de videojuegos e inteligencia artificial
 
## Requerimientos
Para poder participar en el desafío, los equipos deberán contar con:
* Unity 2018.4, versiones más nuevas pueden funcionar pero se recomienda usar esa versión por temas de compatibilidad de proyecto.
* Visual Studio o JetBrains Rider para escribir C#
* Cualquier tipo de cliente git
 
## Instalación
1. [Descarga Unity Hub](https://unity3d.com/get-unity/download/archive)
2. Desde unity Hub, instala la versión 2018.4.x2.
3. Durante la instalación de unity podras agregar Visual Studio Community 2019
4. [Clona el proyecto usando git](https://gitlab.etermax.net/eterchallenge/eter-soccer-challenge)
5. Levanta el proyecto desde unity, no existe el archivo de proyecto Unity, sino que detecta el directorio como proyecto entero.
 
## Prueba un partido
1. Una vez en Unity, usando el visor de proyecto, navega hasta la escena `Assets/Scenes/TeamSelection` y abrela en el editor.
2. Dale click en el botón de play ubicado en la parte central superior del editor!
 
## Construye tu equipo
El proyecto ya incluye dos equipos pre-armados con su código visible para poder basarte en el, estos equipos se encuentran en la ruta `Assets/Teams`, copia todo el directorio llamado `ExampleTeam` para poder crear un equipo nuevo.
 
Dentro del directorio de equipo deberían haber 4 scripts:
- `ExampleTeam.cs`
Este script representa la información general de tu equipo, puedes renombrar la clase y el archivo con el nombre que quieras.
 
- `PlayerOne.cs`
- `PlayerTwo.cs`
- `PlayerThree.cs`
 
Cada uno de estos representa un jugador diferente. Dentro de estos scripts encontrarás métodos que debes implementar para lograr que tus jugadores respondan de manera inteligente a lo que pasa dentro del partido.
En la sección [API](#api) de este documento encontrarás toda la información necesaria para programar a tus jugadores.
Se puede editar el nombre de cualquier jugador para que sea visible durante el partido.
 
Además de esto, necesitamos definir algunas características del equipo:
* Nombre: Podemos asignar el nombre del equipo en el código del mismo, modificando el valor de GetName()
* Escudo: Podemos importar un png a gusto en la ruta `Assets/Teams/Resources/Emblems` y luego asignarlo por nombre en el código del equipo.
* Color Primario: Para definir el color primario podemos definirlo en el código del equipo en la propiedad PrimaryColor. Para definir un nuevo color desde los valores RGB podemos usar el constructor new Color(float r, float g, float b).
 
 
## API
 
## clase TeamPlayer
 
#### Funciones con posibilidad de sobreescritura
 
`OnUpdate()` -> Esta función corre constantemente, es un lugar donde podemos manejar los estados según condiciones dadas por ejemplo.
 
`OnReachBall()` -> Esta función se llama cuando el jugador toca la pelota, de esta manera, por ejemplo, podemos llamar a la función ShootBall especificando fuerza y direccion si queremos que este jugador haga un pase o disparo al arco en seguida entra en contacto con la pelota.
 
`OnScoreBoardChanged(Scoreboard)` ->  Cada vez que alguien anota un gol, esta función se llama, recibiendo el parámetro acorde. Ideal para implementar estrategias según la diferencia de goles.
 
`GetInitialPosition() : FieldPosition` -> Devuelve la posición inicial del jugador al comenzar el partido. Útil si deseas mantener formaciones defensivas.
 
`GetPlayerDisplayName() : string` -> Devuelve el nombre del jugador para mostrar en el partido.
 
#### Funciones públicas
 
`float GetTimeLeft()` -> Devuelve cuánto tiempo resta de partido.
 
`TeamType GetTeamType()` -> Devuelve el tipo de equipo.
 
`int GetMyScore()` -> Devuelve tu marcador.
 
`int GetRivalScore()` -> Devuelve el marcador del rival.
 
`Vector3 GetPosition()` -> Devuelve la posición actual del jugador.
 
`Vector3 GetBallPosition()` -> Devuelve la posición actual de la pelota.
 
`Vector3 GetBallVelocity()` -> Devuelve el vector velocidad de la pelota. Con esto podemos saber que tan rápido se mueve y hacia donde, nos permite hacer cálculos de trayectoria.
 
`Vector3 GetDirectionTo(Vector3)` -> Calcula la dirección hacia una posición dada.
 
`Vector3 GetPositionFor(FieldPosition)` -> Retorna la posición de un cuadrante dado.
 
`void GoTo(FieldPosition)` -> Mueve al jugador hacia el cuadrante específico y se detiene al llegar al mismo.
 
`void MoveBy(Vector3)` -> Mueve al jugador en la dirección dada.
 
`Vector3 GetRivalGoalPosition()` -> Devuelve la posición del arco rival.
 
`Vector3 GetMyGoalPosition()` -> Devuelve la posición del arco propio.
 
`PlayerDTO GetTeamMatesInformation()` -> Devuelve posición, dirección, y velocidad de tus jugadores.
 
`PlayerDTO GetRivalsInformation()` -> Devuelve posición, dirección, y velocidad de los jugadores rivales.
 
`void Stop()` -> Detiene el jugador donde está parado.
 
`string ToString()` -> Imprime la posición, dirección y velocidad del jugador en la consola de unity.
 
## Interfaces
 
### Posición en el campo
 
Interfaz que ayuda a posicionar a los jugadores en el campo:
 
![Alt text](ReadmeResources/field-positions.png?raw=true "Title")
 
### Fuerza de tiro
 
Es posible que tengas que hacer un pase preciso, para eso podemos darle diferente intensidad a los tiros:
 
```csharp
public enum ShootForce
{
   Low = 100, Medium = 450, High = 800
}
```
 
## Ejemplos
 
Mover el jugador hacia la pelota:
 
```csharp
public override void OnUpdate()
{
   var ballPosition = GetBallPosition();
   var directionToBall = GetDirectionTo(ballPosition);
   MoveBy(directionToBall);   
}
```
 
Si el jugador esta cerca de la pelota, moverlo hacia la misma y si no, pararse cerca de la posición A2 (posicion defensiva):
 
```csharp
public override void OnUpdate()
{
   var ballPosition = GetBallPosition();
   if (Vector3.Distance(ballPosition, GetMyGoalPosition()) < 5) // 5 meters in unity units
   {
       MoveBy(GetDirectionTo(ballPosition));
   }
   else
   {
       GoTo(FieldPosition.A2);
   }
}
```
 
Disparar hacia el arco opuesto:
 
```csharp
public override void OnReachBall()
{
   var rivalGoalPosition = GetRivalGoalPosition();
   var directionToRivaGoal = GetDirectionTo(rivalGoalPosition);
   ShootBall(directionToRivaGoal, ShootForce.High);
}
```
 
Hacer algo según el marcador:
 
```csharp
public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
{
   if (scoreBoard.My < scoreBoard.Rival)
   {
       // losing
   }
   else if (scoreBoard.My == scoreBoard.Rival)
   {
       // drawing
   }
   else
   {
       // winning
   }
}
```
 
 

